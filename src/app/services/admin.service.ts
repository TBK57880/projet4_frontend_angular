import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../entities/user';
import { Categoryclaim } from '../entities/categoryclaim';
import { Statusclaim } from '../entities/statusclaim';
import { Claim } from '../entities/claim';
import { Hotels } from '../entities/hotels';

// let API_URL = "http://localhost:9999/api/admin/";
let API_URL = "https://projet4-spring-mysql-prod.herokuapp.com/admin/";

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  // public currentUser: Observable<User>;
  headers: HttpHeaders;
  private currentUserSubject: BehaviorSubject<User>;

  get currentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      authorization:'Bearer ' + this.currentUser?.token,
      "Content-Type":"application/json; charset=UTF-8"
    });
  }

  isAdmin(){
    return this.currentUser.role === 'ADMIN';
  }

  login(user: User): Observable<any> {
    const headers = new HttpHeaders(user ? {
      authorization:'Basic ' + btoa(user.userName + ':' + user.password)
    }:{});

    return this.http.get<any> (API_URL + "login", {headers:headers}).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
          this.currentUserSubject.next(response);
        }
        return response;
      })
    );
  }

  logOut(): Observable<any> {
    return this.http.post(API_URL + "logout", {}).pipe(
      map(response => {
        if(response) {
          localStorage.removeItem('currentUser');
          this.currentUserSubject.next(null);
        }
        return response;
      })
    );
  }

  register(user: User): Observable<any> {
    return this.http.post(API_URL + "registration", JSON.stringify(user),
  {headers: {"Content-Type":"application/json; charset=UTF-8"}});
  }

  getAllUsers(): Observable<any>{
    return this.http.get(API_URL + "allUsers"
    // ,{headers: this.headers}
    );
  }

  getNumberOfUsers(): Observable<any>{
    return this.http.get(API_URL + "numberOfUsers");
  }

  getAllClaims(): Observable<any> {
    return this.http.get(API_URL + "allClaims");
  }

  getUserByUsername(username: string): Observable<any>{
    return this.http.get(API_URL + "fectchUserByUserName/" + username);
  }

  getAllClaimsByTitle(title: string): Observable<any>{
    return this.http.get(API_URL + "fetchAllClaimsByTitle/" + title);
  }

  getAllClaimsByDescription(description: string): Observable<any>{
    return this.http.get(API_URL + "fetchAllClaimsByTitle/" + description);
  }

  getAllClaimsByDescriptionAndTitle(title: string, description: string): Observable<any>{
    return this.http.get(API_URL + "fetchAllClaimsByTitle/" + title + "/" + description);
  }

  getAllClaimsByCategory(categoryclaim: Categoryclaim): Observable<any>{
    return this.http.get(API_URL + "fetchAllClaimsByTitle/" + categoryclaim);
  }

  getAllClaimsByStatus(statusclaim: Statusclaim): Observable<any>{
    return this.http.get(API_URL + "fetchClaimByStatusClaim/" + statusclaim);
  }

  fetchHotelsUsersByUsername(username: string): Observable<any>{
    return this.http.get(API_URL + "fetchHotelsUsersByUsername/" + username);
  }


  // ---------------------put--------------------------

  changeStatusClaimDuringTreatment(id: number, claim: Claim): Observable<Object> {
    return this.http.put(API_URL + "changeStatusClaimDuringTreatment/" +id, claim);
  }

  changeStatusClaimProcessed(id: number, claim: Claim): Observable<Object> {
    return this.http.put(API_URL + "changeStatusClaimProcessed/" +id, claim);
  }

  deleteUser(id: Number): Observable<any> {
    return this.http.delete(API_URL + "deleteUser/" + id, {headers: {"Content-Type":"application/json"}});
  }



}

