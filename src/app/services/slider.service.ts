import { Injectable } from '@angular/core';
import Swiper from 'swiper';

@Injectable({
  providedIn: 'root'
})
export class SliderService {


galleryThumbs:any = new Swiper('.gallery-thumbs', {
  spaceBetween: 0,
  slidesPerView: 0,
});
galleryTop:any = new Swiper('.gallery-top', {
  effect: 'fade',
  loop: true,
  thumbs: {
    swiper: this.galleryThumbs
  }
});

linkYoutube = [
  "https://www.youtube.com/embed/4tPG4z2HeXg",
  "https://www.youtube.com/embed/rUldAQ2Haqo",
  "https://www.youtube.com/embed/0NwFVXfSCCw",
  "https://www.youtube.com/embed/BsxcNhg4D3Y",
  "https://www.youtube.com/embed/t3sn-EQFVWk",
  "https://www.youtube.com/embed/wy7feYsWWP4"
]


  constructor() { }
}

