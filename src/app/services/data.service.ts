import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';

let API_URL = "https://projet4-nodejs-mongodb-prod.herokuapp.com";
// http://localhost:5500

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private _http: HttpClient) { }


  verifyConnection(){
    return this._http.get('/verifyConnection');
  }

  getData_WeatherOfTheDayBySpot(): Observable<any>{
    return this._http.get<any>(API_URL + '/weatherofthedays');
  }

  getData_ForecastWeatherFor7DaysBySpot(): Observable<any>{
    return this._http.get<any>(API_URL + '/forecast7days');
  }

  getAllData_HotelsBySpot(): Observable<any>{
    return this._http.get<any>(API_URL + '/hotels');
  }
  
  getData_Hotels_Tongariro(): Observable<any>{
    return this._http.get<any>(API_URL + '/spot/Tongariro');
  }

  getData_Hotels_Dubrovnik(): Observable<any>{
    return this._http.get<any>(API_URL + '/spot/Dubrovnik');
  }

  getData_Hotels_Matmata(): Observable<any>{
    return this._http.get<any>(API_URL + '/spot/Matmata');
  }

  getData_ForecastWeatherFor7DaysBySpot_Map(): Observable<any>{
    return this._http.get<any>(API_URL + '/forecast7days').pipe(map(response => {
      if(response) {
       console.log("response with map is ok")
      }
      return response;
    })
    );
  }
  // verifyConnection(){
  //   return this._http.get('http://localhost:5500/verifyConnection');
  // }

  // getData_WeatherOfTheDayBySpot(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/weatherofthedays');
  // }

  // getData_ForecastWeatherFor7DaysBySpot(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/forecast7days');
  // }

  // getAllData_HotelsBySpot(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/hotels');
  // }
  
  // getData_Hotels_Tongariro(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/hotels/spot/Tongariro');
  // }

  // getData_Hotels_Dubrovnik(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/hotels/spot/Dubrovnik');
  // }

  // getData_Hotels_Matmata(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/hotels/spot/Matmata');
  // }

  // getData_ForecastWeatherFor7DaysBySpot_Map(): Observable<any>{
  //   return this._http.get<any>('http://localhost:5500/forecast7days').pipe(map(response => {
  //     if(response) {
  //      console.log("response with map is ok")
  //     }
  //     return response;
  //   })
  //   );
  // }

  

}
