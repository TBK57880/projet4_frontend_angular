import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../entities/user';
import { Claim } from '../entities/claim';
import { Categoryclaim } from '../entities/categoryclaim';
import { Statusclaim } from '../entities/statusclaim';
import { Hotels } from '../entities/hotels';

// let API_URL = "http://localhost:9999/user/";
let API_URL = "https://projet4-spring-mysql-prod.herokuapp.com/user/";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private headers: HttpHeaders;
  private currentUserSubject: BehaviorSubject<User>;

  get currentUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  constructor(private http: HttpClient) {
    const token = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUserSubject = new BehaviorSubject<User> (token);
    this.headers = new HttpHeaders({
      authorization:'Bearer ' + token,
      "Content-Type":"application/json; charset=UTF-8",
      // 'Access-Control-Allow-Origin':'*'
    });
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(user: User): Observable<any> {
    const headers = new HttpHeaders(user ? {
      authorization:'Basic ' + btoa(user.userName + ':' + user.password), 
      // 'Access-Control-Allow-Origin':'*'
    }:{});

    return this.http.get<any> (API_URL + "login", {headers:headers}).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
          this.currentUserSubject.next(response);
        }
        return response;
      })
    );
  }

  logOut(): Observable<any> {
    return this.http.post(API_URL + "logout", {}).pipe(
      map(response => {
        if(response) {
          localStorage.removeItem('currentUser');
          this.currentUserSubject.next(null);
        }
        return response;
      })
    );
  }

  register(user: User): Observable<any> {
    return this.http.post(API_URL + "registration", JSON.stringify(user),
  {headers: {"Content-Type":"application/json; charset=UTF-8", 
  // 'Access-Control-Allow-Origin':'*'
}});
  }

  updateUser(id: number, user:User): Observable<any> {
    return this.http.put(API_URL + "updateUser/" + id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  updatePasswordUser(id: number, user:User): Observable<any> {
    console.log(user.password);
    return this.http.put(API_URL + "updatePasswordUser/" + id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }


  addClaim(id: number, claim: Claim): Observable<Object> {
    console.log(id);
    console.log(claim);
    return this.http.post(API_URL + "addClaim/" +id, claim)
  }

  addHotel(hotel: Hotels, id: number): Observable<Object> {
    console.log(id);
    console.log(hotel);
    return this.http.post(API_URL + "addHotel/" +id, hotel);
  }

// ---------------------get--------------------------


  // getAllUsers(): Observable<any>{
  //   return this.http.get(API_URL + "allUsers");
  // }

  // getNumberOfUsers(): Observable<any>{
  //   return this.http.get(API_URL + "numberOfUsers");
  // }

  // getAllClaims(): Observable<any> {
  //   return this.http.get(API_URL + "allClaims");
  // }


  // getAllHotels(): Observable<any> {
  //   return this.http.get(API_URL + "allHotels");
  // }

  // getUserByUsername(username: string): Observable<any>{
  //   return this.http.get(API_URL + "fectchUserByUserName/" + username);
  // }

  // getAllClaimsByTitle(title: string): Observable<any>{
  //   return this.http.get(API_URL + "fetchAllClaimsByTitle/" + title);
  // }

  // getAllClaimsByDescription(description: string): Observable<any>{
  //   return this.http.get(API_URL + "fetchAllClaimsByTitle/" + description);
  // }

  // getAllClaimsByDescriptionAndTitle(title: string, description: string): Observable<any>{
  //   return this.http.get(API_URL + "fetchAllClaimsByTitle/" + title + "/" + description);
  // }

  // getAllClaimsByCategory(categoryclaim: Categoryclaim): Observable<any>{
  //   return this.http.get(API_URL + "fetchAllClaimsByTitle/" + categoryclaim);
  // }

  // getAllClaimsByStatus(statusclaim: Statusclaim): Observable<any>{
  //   return this.http.get(API_URL + "fetchClaimByStatusClaim/" + statusclaim);
  // }

  // fetchHotelsUsersByUsername(username: string): Observable<any>{
  //   return this.http.get(API_URL + "fetchHotelsUsersByUsername/" + username);
  // }

  // ---------------------put--------------------------

  // changeStatusClaimDuringTreatment(id: number, claim: Claim): Observable<Object> {
  //   return this.http.put(API_URL + "changeStatusClaimDuringTreatment/" +id, claim);
  // }

  // changeStatusClaimProcessed(id: number, claim: Claim): Observable<Object> {
  //   return this.http.put(API_URL + "changeStatusClaimProcessed/" +id, claim);
  // }

// ---------------Favorite

  isVisibleFavorite0(id:number, user: User): Observable<Object> {
    return this.http.put(API_URL + "isVisibleFavorite0/" + id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  isVisibleFavorite1(id:number, user: User): Observable<Object> {
    return this.http.put(API_URL + "isVisibleFavorite1/" + id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  isVisibleFavorite2(id:number, user: User): Observable<Object> {
    return this.http.put(API_URL + "isVisibleFavorite2/" + id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  isNotVisibleFavorite0(id:number, user: User): Observable<Object> {
    return this.http.put(API_URL + "isNotVisibleFavorite0/" +id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  isNotVisibleFavorite1(id:number, user: User): Observable<Object> {
    return this.http.put(API_URL + "isNotVisibleFavorite1/" +id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  isNotVisibleFavorite2(id:number, user: User): Observable<Object> {
    return this.http.put(API_URL + "isNotVisibleFavorite2/" +id, user).pipe(
      map(response => {
        if(response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      })
    );
  }

  // ---------------------delete--------------------------

  // deleteUser(id: Number): Observable<any> {
  //   return this.http.delete(API_URL + "deleteUser/" + id, {headers: {"Content-Type":"application/json"}});
  // }

  deleteHotel(id: Number): Observable<any> {
    return this.http.delete(API_URL + "deleteHotel/" + id, {headers: {"Content-Type":"application/json"}});
  }



  

}
