import { User } from './../entities/user';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-registration-login',
  templateUrl: './registration-login.component.html',
  styleUrls: ['./registration-login.component.css']
})
export class RegistrationLoginComponent implements OnInit {

  public user: User = new User();
  msg='';
  
  constructor(private _service: UserService, private _router: Router) { }

  ngOnInit() {
    if(this._service.currentUserValue) {
      this._router.navigate(['/slider-spot']);
      return;
    }

    const sign_in_btn = document.querySelector("#sign-in-btn");
    const sign_up_btn = document.querySelector("#sign-up-btn");
    const container = document.querySelector(".container");
    sign_up_btn.addEventListener("click", () => {
      container.classList.add("sign-up-mode");
    });
    
    sign_in_btn.addEventListener("click", () => {
      container.classList.remove("sign-up-mode");
    });
  }

  register(){
    this._service.register(this.user).subscribe(
      data => {
        console.log("response recieved");
        this.msg="Registration successfully";
      },
      error => {
        console.log("exception occured");
        this.msg=error.error;
      } 
    )
  }

  login(){
    this._service.login(this.user).subscribe(
      data => {
        console.log("response recieved");
        this._router.navigate(['/slider-spots']);
      },
      error => {
        console.log("exception occured");
        this.msg="Bad credentials, please enter valid username and password";
      } 
    )
  }

}
