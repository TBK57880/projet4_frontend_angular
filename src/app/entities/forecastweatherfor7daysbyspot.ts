export class Forecastweatherfor7daysbyspot {
    id: Number;
    spot: String;
    country: String;
    movie: String;
    lat: Number;
    lon: Number;
    urlImageSpot: String;
    forecast_weather: [
        {
          date_time: String,
          temp_day: Number,
          temp_min: Number,
          temp_max: Number,
          description: String,
          feels_like: Number,
          pressure: Number,
          humidity: Number,
          wind_speed: Number,
          weather_main: String,
      }
    ];
    favorite_state: Boolean;
}
