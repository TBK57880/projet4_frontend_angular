export class Hotelsbyspot {
    id: Number;
    spot: String;
    country: String;
    date_checkout: String;
    date_checkin: String;
    results_hotels: [
        {
          name: String,
          hotelId: Number,
          starRating: Number,
          streetAddress: String,
          postalCode: String,
          locality: String,
          guestReviews: String,
          lat: Number,
          lon: Number,
          distanceBetweenSpotAndHotel: Number,
          price: Number,
      }
    ]
}
