export enum Categoryclaim {
    CHARTS = "CHARTS",
     MAPS = "MAPS",
    DASHBOARD = "DASHBOARD",
    HOTELS = "HOTELS",
    INTERFACE = "INTERFACE",
    APPLICATION = "APPLICATION",
    ACCOUNT = "ACCOUNT",
    OTHER = "OTHER",
}
