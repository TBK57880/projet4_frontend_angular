import { Hotels } from './hotels';
import { Claim } from './claim';
import { Role } from './role';

export class User {
  id_user: number;
  userName: string;
  email: string;
  phone: string;
  password: string;
  addressLine1: string;
  addressLine2: string;
  postalCode: string;
  city: string;
  favorite0_isVisible: boolean;
  favorite1_isVisible: boolean;
  favorite2_isVisible: boolean;
  role: Role;
  claim: Claim;
  token: string;
  hotels: Hotels;
  
}
