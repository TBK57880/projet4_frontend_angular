export class Weatherofthedaybyspot {
  id: Number;
  spot: String;
  country: String;
  movie: String;
  temperature: Number;
  description: String;
  date_time: String;
  temp_min: Number;
  temp_max: Number;
  pressure: Number;
  humidity: Number;
  wind_speed: Number;
  weather_main: String;
  urlImageSpot: String;
  favorite_state: Boolean;
}
