import { Categoryclaim } from "./categoryclaim";
import { Statusclaim } from "./statusclaim";
import { User } from "./user";

export class Claim {
    id_claim: number;
    title: string;
    description: string;
    categoryclaim : Categoryclaim;
    statusclaim: Statusclaim;
    user: User;
}
