import { User } from 'src/app/entities/user';

export class Hotels {
    hotelId: Number;
    name: String;
    starRating: Number;
    streetAddress: String;
    postalCode: String;
    locality: String;
    guestReviews: String;
    distanceBetweenSpotAndHotel: Number;
    price: Number;
    user: User;
    percent: Number;
}
