import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { RouterModule } from '@angular/router';
import { PostsComponent } from 'src/app/modules/posts/posts.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardFavoritesComponent } from 'src/app/modules/dashboard-favorites/dashboard-favorites.component';
import {MatTableModule} from '@angular/material/table';
import { DashboardSettingsComponent } from 'src/app/modules/dashboard-settings/dashboard-settings.component';
import { DashboardHotelsComponent } from 'src/app/modules/dashboard-hotels/dashboard-hotels.component';
import { DashboardMapWeatherComponent } from 'src/app/modules/dashboard-map-weather/dashboard-map-weather.component';
import { DashboardMapPlaneComponent } from 'src/app/modules/dashboard-map-plane/dashboard-map-plane.component';
import { FormsModule } from '@angular/forms';
import { DashboardJaugeComponent } from 'src/app/modules/dashboard-jauge/dashboard-jauge.component';
import { DashboardChartTempDailyComponent } from 'src/app/modules/dashboard-chart-temp-daily/dashboard-chart-temp-daily.component';
import { DashboardChartCupHumidityComponent } from 'src/app/modules/dashboard-chart-cup-humidity/dashboard-chart-cup-humidity.component';
import { DashboardChartTopSpotComponent } from 'src/app/modules/dashboard-chart-top-spot/dashboard-chart-top-spot.component';
import { DashboardClaimComponent } from 'src/app/modules/dashboard-claim/dashboard-claim.component';
import { DashboardAdminUsersComponent } from 'src/app/modules/dashboard-admin-users/dashboard-admin-users.component';
import { DashboardAdminClaimsComponent } from 'src/app/modules/dashboard-admin-claims/dashboard-admin-claims.component';



@NgModule({
  declarations: [
    DefaultComponent,
    DashboardFavoritesComponent,
    DashboardSettingsComponent,
    DashboardHotelsComponent,
    DashboardMapWeatherComponent,
    DashboardMapPlaneComponent,
    DashboardJaugeComponent,
    DashboardChartTempDailyComponent,
    DashboardChartCupHumidityComponent,
    DashboardChartTopSpotComponent,
    DashboardClaimComponent,
    DashboardAdminUsersComponent,
    DashboardAdminClaimsComponent,
    PostsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatTableModule,
    FormsModule,
    
    
  ],
  providers : [
  ],
})
export class DefaultModule { }