import { path, percent } from '@amcharts/amcharts4/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hotels } from 'src/app/entities/hotels';
import { Hotelsbyspot } from 'src/app/entities/hotelsbyspot';
import { User } from 'src/app/entities/user';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-hotels',
  templateUrl: './dashboard-hotels.component.html',
  styleUrls: ['./dashboard-hotels.component.css']
})
export class DashboardHotelsComponent implements OnInit {

  msg='';
  

  hotels_Dubrovnik: Hotelsbyspot;
  hotels_Matmata: Hotelsbyspot;
  hotels_Tongariro: Hotelsbyspot;
  allhotelsbyspots: Hotelsbyspot[] = [];

  data_hotels_Dubrovnik: Hotels[] = [];
  data_hotels_Matmata: Hotels[] = [];
  data_hotels_Tongariro: Hotels[] = [];

  constructor(private userService: UserService, private dataService : DataService) { }



  ngOnInit() {
    this.getAllData_HotelsBySpot_WithDataService();
    this.getDataDubrovnik_HotelsBySpot_WithDataService();
    this.getDataTongariro_HotelsBySpot_WithDataService();
    this.getDataMatmata_HotelsBySpot_WithDataService();
  }

  getAllData_HotelsBySpot_WithDataService(){
    this.dataService.getAllData_HotelsBySpot().subscribe((response) =>{
      // console.log(response);
      this.allhotelsbyspots = response;
    });
  }

  getDataDubrovnik_HotelsBySpot_WithDataService(){
    this.dataService.getData_Hotels_Dubrovnik().subscribe((response) =>{
      console.log(response);
      this.hotels_Dubrovnik = response;
      this.data_hotels_Dubrovnik = this.transleteDataToHotels(this.hotels_Dubrovnik);
      console.log(this.data_hotels_Dubrovnik);
    });
  }
  

  getDataMatmata_HotelsBySpot_WithDataService(){
    this.dataService.getData_Hotels_Matmata().subscribe((response) =>{
      // console.log(response);
      this.hotels_Matmata = response;
      this.data_hotels_Matmata = this.transleteDataToHotels(this.hotels_Matmata);
      // console.log(this.data_hotels_Matmata);
    });
  }

  getDataTongariro_HotelsBySpot_WithDataService(){
    this.dataService.getData_Hotels_Tongariro().subscribe((response) =>{
      // console.log(response);
      this.hotels_Tongariro = response;
      this.data_hotels_Tongariro = this.transleteDataToHotels(this.hotels_Tongariro);
      // console.log(this.data_hotels_Tongariro);
    });
  }

  transleteDataToHotels(hotels_spot: Hotelsbyspot){
    // console.log(hotels_spot);
    // console.log(hotels_spot.results_hotels.length);
    var data: Hotels[] = [];
      for(var i = 0; i < hotels_spot.results_hotels.length; i++)
      {
        // console.log("-------step1---------");
        var percent_hotel = 0;
        if (hotels_spot.results_hotels[i].starRating == 5) {
          percent_hotel += 50;
        } else if (hotels_spot.results_hotels[i].starRating == 4) {
          percent_hotel += 40;
        } else if (hotels_spot.results_hotels[i].starRating == 3) {
          percent_hotel += 30;
        } else if (hotels_spot.results_hotels[i].starRating == 2) {
          percent_hotel += 20;
        } else if (hotels_spot.results_hotels[i].starRating == 1) {
          percent_hotel += 10;
        }
        
        if (hotels_spot.results_hotels[i].guestReviews == "fabulous") {
          percent_hotel += 10;
        } else if (hotels_spot.results_hotels[i].guestReviews == "exceptional") {
          percent_hotel += 20;
        } else if (hotels_spot.results_hotels[i].guestReviews == "good") {
          percent_hotel += 30;
        }
          if (hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel > 30 ) {
            percent_hotel += 0;
          } else if (hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel <= 30 && hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel > 20) {
            percent_hotel += 10;
          } else if (hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel <= 20 && hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel > 10) {
            percent_hotel += 20;
          } else if (hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel <= 10) {
            percent_hotel += 30;
          }

          console.log(percent_hotel);

        data[i] = 
          {
            name: hotels_spot.results_hotels[i].name,
            hotelId: hotels_spot.results_hotels[i].hotelId,
            starRating: hotels_spot.results_hotels[i].starRating,
            streetAddress: hotels_spot.results_hotels[i].streetAddress,
            postalCode: hotels_spot.results_hotels[i].postalCode,
            locality: hotels_spot.results_hotels[i].locality,
            guestReviews: hotels_spot.results_hotels[i].guestReviews,
            distanceBetweenSpotAndHotel: hotels_spot.results_hotels[i].distanceBetweenSpotAndHotel,
            price: hotels_spot.results_hotels[i].price,
            user: undefined,
            percent: percent_hotel,
            };
            // console.log("-------step2---------");  
      };
      // console.log(data[0]);
      return data;
  }

  saveHotel(_hotel: Hotels){
    console.log(_hotel);
    this.userService.addHotel(_hotel, this.userService.currentUser.id_user,).subscribe((response: any) => {
      console.log(response);
    });
  }



}
