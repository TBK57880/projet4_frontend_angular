import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardMapPlaneComponent } from './dashboard-map-plane.component';

describe('DashboardMapPlaneComponent', () => {
  let component: DashboardMapPlaneComponent;
  let fixture: ComponentFixture<DashboardMapPlaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardMapPlaneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardMapPlaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
