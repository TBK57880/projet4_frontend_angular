import { Component, Inject, NgZone, OnInit, PLATFORM_ID } from '@angular/core';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldHigh from '@amcharts/amcharts4-geodata/worldHigh';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { isPlatformBrowser } from '@angular/common';
// import { percent } from '@amcharts/amcharts4/core';
import { User } from 'src/app/entities/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-chart-cup-humidity',
  templateUrl: './dashboard-chart-cup-humidity.component.html',
  styleUrls: ['./dashboard-chart-cup-humidity.component.css']
})
export class DashboardChartCupHumidityComponent implements OnInit {

  msg='';

  private chart: am4charts.XYChart;

  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[] = [];
  spot_SelectOptions: any[] = [];
  date_SelectOptions: any[] = [];
  date_selected: number = 0;
  spot_selected: number = 0;
  datetime: String;
  

  table_visibility: any[] = [];


  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private zone: NgZone,
    private dataService: DataService,
    private userService: UserService,
  ) {}

  ngOnInit() {
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService() {
    this.dataService
      .getData_ForecastWeatherFor7DaysBySpot()
      .subscribe((response) => {
        console.log(response);
        this.forecastweatherfor7daysbyspots = response;

        // date_SelectOptions

        for (var i = 0; i < this.forecastweatherfor7daysbyspots[0].forecast_weather.length;i++) {
          this.date_SelectOptions[i] = {
            name: this.forecastweatherfor7daysbyspots[0].forecast_weather[i]
              .date_time,
            id: i,
          };
          // console.log(this.date_SelectOptions[i].value);
        }
        this.datetime =
          this.forecastweatherfor7daysbyspots[0].forecast_weather[
            this.date_selected
          ].date_time;

          // spot_SelectOptions

          // console.log(this.currentUser.favorite0_isVisible);
          // console.log(this.currentUser.favorite1_isVisible);
          // console.log(this.currentUser.favorite2_isVisible);

          this.tableVisibilityChanged();
          // console.log(this.table_visibility[0]);

          for (var i = 0; i < this.forecastweatherfor7daysbyspots.length;i++) {
            if (this.table_visibility[i] == "v" ) {
              this.spot_SelectOptions[i] = {
                name: this.forecastweatherfor7daysbyspots[i].spot,
                id: i,
              };
               console.log(this.spot_SelectOptions[i]);
            } else {
              this.spot_SelectOptions[i] = {
                name: this.msg,
                id: i,
              };
              // console.log(this.spot_SelectOptions[i]);
            }
            
          }
          this.createChart(this.forecastweatherfor7daysbyspots, this.date_selected, this.spot_selected);

      });
  }

  tableVisibilityChanged(){
    if(this.userService.currentUser.favorite0_isVisible == true) {
      this.table_visibility[0] = 'v';
    } else {
      this.table_visibility[0] = 'nv'
    }

    if(this.userService.currentUser.favorite1_isVisible == true) {
      this.table_visibility[1] = 'v';
    } else {
      this.table_visibility[1] = 'nv'
    }

    if(this.userService.currentUser.favorite2_isVisible == true) {
      this.table_visibility[2] = 'v';
    } else {
      this.table_visibility[2] = 'nv'
    }
  }

  changeOptionDate(_id_date: number) {
    this.createChart(this.forecastweatherfor7daysbyspots, _id_date, this.spot_selected);
    this.datetime =
    this.forecastweatherfor7daysbyspots[0].forecast_weather[_id_date].date_time;
  }

  changeOptionSpot(_id_spot: number) {
    console.log(this.spot_SelectOptions[_id_spot]);
    // const spot = this.spot_SelectOptions.find(s => s.id === _id_spot && s.name !==);
    if (_id_spot == this.spot_SelectOptions[_id_spot].id && this.spot_SelectOptions[_id_spot].name != this.msg) {
      this.createChart(this.forecastweatherfor7daysbyspots, this.date_selected, _id_spot);
    }
  }

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  createChart(_forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[],
    _id_date: number, _id_spot: number) {
    this.browserOnly(() => {

am4core.useTheme(am4themes_animated);


let iconPath = "M421.976,136.204h-23.409l-0.012,0.008c-0.19-20.728-1.405-41.457-3.643-61.704l-1.476-13.352H5.159L3.682,74.507 C1.239,96.601,0,119.273,0,141.895c0,65.221,7.788,126.69,22.52,177.761c7.67,26.588,17.259,50.661,28.5,71.548  c11.793,21.915,25.534,40.556,40.839,55.406l4.364,4.234h206.148l4.364-4.234c15.306-14.85,29.046-33.491,40.839-55.406  c11.241-20.888,20.829-44.96,28.5-71.548c0.325-1.127,0.643-2.266,0.961-3.404h44.94c49.639,0,90.024-40.385,90.024-90.024  C512,176.588,471.615,136.204,421.976,136.204z M421.976,256.252h-32c3.061-19.239,5.329-39.333,6.766-60.048h25.234  c16.582,0,30.024,13.442,30.024,30.024C452,242.81,438.558,256.252,421.976,256.252z"

let chart = am4core.create("chartdiv_cup", am4charts.SlicedChart);
chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
chart.paddingLeft = 150;

let aridity: Number = 100;


chart.data = [{
    "name": "Aridity",
    "value": 40,
    "disabled":true
}, {
    "name": "Humidity",
    "value": _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].humidity
}];

let series = chart.series.push(new am4charts.PictorialStackedSeries());
series.dataFields.value = "value";
series.dataFields.category = "name";
series.alignLabels = true;
series.labels.template.propertyFields.disabled = "disabled";
series.ticks.template.propertyFields.disabled = "disabled";


series.maskSprite.path = iconPath;
series.ticks.template.locationX = 1;
series.ticks.template.locationY = 0;

series.labelsContainer.width = 100;

chart.legend = new am4charts.Legend();
chart.legend.position = "top";
chart.legend.paddingRight = 160;
chart.legend.paddingBottom = 40;
let marker: any = chart.legend.markers.template.children.getIndex(0);
chart.legend.markers.template.width = 40;
chart.legend.markers.template.height = 40;
marker.cornerRadius(20,20,20,20);

    });
  }

  ngOnDestroy() {
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

}

  

