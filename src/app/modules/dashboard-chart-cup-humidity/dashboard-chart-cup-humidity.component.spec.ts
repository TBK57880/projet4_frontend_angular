import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardChartCupHumidityComponent } from './dashboard-chart-cup-humidity.component';

describe('DashboardChartCupHumidityComponent', () => {
  let component: DashboardChartCupHumidityComponent;
  let fixture: ComponentFixture<DashboardChartCupHumidityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardChartCupHumidityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardChartCupHumidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
