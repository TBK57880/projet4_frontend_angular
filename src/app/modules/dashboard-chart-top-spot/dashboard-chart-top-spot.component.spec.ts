import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardChartTopSpotComponent } from './dashboard-chart-top-spot.component';

describe('DashboardChartTopSpotComponent', () => {
  let component: DashboardChartTopSpotComponent;
  let fixture: ComponentFixture<DashboardChartTopSpotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardChartTopSpotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardChartTopSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
