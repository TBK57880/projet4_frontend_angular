import { Component, Inject, NgZone, OnInit, PLATFORM_ID } from '@angular/core';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldHigh from '@amcharts/amcharts4-geodata/worldHigh';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { isPlatformBrowser } from '@angular/common';
import { percent } from '@amcharts/amcharts4/core';
import { User } from 'src/app/entities/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-chart-top-spot',
  templateUrl: './dashboard-chart-top-spot.component.html',
  styleUrls: ['./dashboard-chart-top-spot.component.css']
})
export class DashboardChartTopSpotComponent implements OnInit {

  msg='';

  private chart: am4charts.XYChart;

  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[] = [];

  test1 = true;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private zone: NgZone,
    private dataService: DataService,
    private userService: UserService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
    if(!this.userService.currentUser){
      this.router.navigate(['/registration-login']);
    } else {
      this.router.navigate(['/dashboard/chart-temp-daily'])
    }
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService() {
    this.dataService
      .getData_ForecastWeatherFor7DaysBySpot()
      .subscribe((response) => {
        // console.log(response);
        this.forecastweatherfor7daysbyspots = response;
        var percent_Tongariro = this.calculatePercentsChart(this.forecastweatherfor7daysbyspots, 0);
        var percent_Dubrovnik = this.calculatePercentsChart(this.forecastweatherfor7daysbyspots, 1);
        var percent_Matmata = this.calculatePercentsChart(this.forecastweatherfor7daysbyspots, 2);
        this.createChart(percent_Tongariro, percent_Matmata, percent_Dubrovnik, this.forecastweatherfor7daysbyspots);

      });
  }

  calculatePercentsChart(_forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[], i: number) {

    var total_percent = 0;

      for (let j = 0; j < _forecastweatherfor7daysbyspots[i].forecast_weather.length; j++) {

        var percent = 0;

        if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day > 5 && _forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day <= 10) {
          percent += 5;
        } else if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day > 10 && _forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day <= 15) {
          percent += 10;
        } else if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day > 15 && _forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day <= 20) {
          percent += 15;
        } else if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day > 20 && _forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day <= 25) {
          percent += 20;
        } else if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day > 25 && _forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day <= 30) {
          percent += 25;
        } else if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].temp_day > 30) {
          percent += 30;
        }
  
         if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].weather_main.includes("Rain")) {
            percent += 10;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("Clouds")) {
            percent += 20;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("Clear")) {
            percent += 30;
         }
  
  
          if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("light rain")) {
            percent += 10;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("few clouds")) {
            percent += 15;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("broken clouds")) {
            percent += 20;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("scattered clouds")) {
            percent += 25;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("overcast clouds")) {
            percent += 30;
          } else  if(_forecastweatherfor7daysbyspots[i].forecast_weather[j].description.includes("clear sky")) {
            percent += 40;
         }

         total_percent += percent;
      }
    // console.log(total_percent);
    total_percent = Math.round(total_percent / _forecastweatherfor7daysbyspots[i].forecast_weather.length);
    // console.log(total_percent);
    return total_percent;

}

changeBulletChart(_percent: number) {
  var bullet = "";
  if(_percent > 0 && _percent <= 30) {
    bullet = "../../assets/images/thumb-down.png";
  } else if (_percent > 30 && _percent <= 50) {
    bullet = "../../assets/images/removed.png";
  } else if (_percent > 50 && _percent <= 100) {
    bullet = "../../assets/images/thumb-up.png";
  }
  return bullet;
}

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  createChart(_percent_Tongariro: number, _percent_Matmata: number, _percent_Dubrovnik: number, _forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[]) {
    this.browserOnly(() => {
    
am4core.useTheme(am4themes_animated);

let chart = am4core.create("chartdiv_chart", am4charts.XYChart);

let bullet_Tongariro = this.changeBulletChart(_percent_Tongariro);
let bullet_Dubrovnik = this.changeBulletChart(_percent_Dubrovnik);
let bullet_Matmata = this.changeBulletChart(_percent_Matmata);

chart.data = [{
    "name": "score: " + _forecastweatherfor7daysbyspots[0].spot,
    "points": _percent_Tongariro,
    "color": chart.colors.next(),
    "bullet": bullet_Tongariro,
}, {
    "name": "score: " + _forecastweatherfor7daysbyspots[1].spot,
    "points": _percent_Dubrovnik,
    "color": chart.colors.next(),
    "bullet": bullet_Dubrovnik
}, {
    "name": "score: " + _forecastweatherfor7daysbyspots[2].spot,
    "points": _percent_Matmata,
    "color": chart.colors.next(),
    "bullet": bullet_Matmata
}];


let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "name";
categoryAxis.renderer.grid.template.disabled = true;
categoryAxis.renderer.minGridDistance = 30;
categoryAxis.renderer.inside = true;
categoryAxis.renderer.labels.template.fill = am4core.color("#fff");
categoryAxis.renderer.labels.template.fontSize = 20;

let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.grid.template.strokeDasharray = "4,4";
valueAxis.renderer.labels.template.disabled = true;
valueAxis.min = 0;


chart.maskBullets = false;


chart.paddingBottom = 0;


let series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.valueY = "points";
series.dataFields.categoryX = "name";
series.columns.template.propertyFields.fill = "color";
series.columns.template.propertyFields.stroke = "color";
series.columns.template.column.cornerRadiusTopLeft = 15;
series.columns.template.column.cornerRadiusTopRight = 15;
series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/b]";


let bullet = series.bullets.push(new am4charts.Bullet());
let image = bullet.createChild(am4core.Image);
image.horizontalCenter = "middle";
image.verticalCenter = "bottom";
image.dy = 20;
image.y = am4core.percent(100);
image.propertyFields.href = "bullet";
image.tooltipText = series.columns.template.tooltipText;
image.propertyFields.fill = "color";
image.filters.push(new am4core.DropShadowFilter());
    });
  }

  ngOnDestroy() {
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

}
