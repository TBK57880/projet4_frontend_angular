import { Component, OnInit } from '@angular/core';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { Weatherofthedaybyspot } from 'src/app/entities/weatherofthedaybyspot';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  displayedColumns = ['ID', 'Spot', 'Country', 'Movie', 'Temperature', 'Description', 'Favorite_State'];
  displayedColumns_2 = ['ID', 'Spot', 'Country', 'Movie', 'Date_0', 'Temperature_0', 'Description_0', 'Date_1', 'Temperature_1', 'Description_1',
  'Date_2', 'Temperature_2', 'Description_2', 'Date_3', 'Temperature_3', 'Description_3', 'Date_4', 'Temperature_4', 'Description_4',
  'Date_5', 'Temperature_5', 'Description_5', 'Date_6', 'Temperature_6', 'Description_6'];

  weatherofthedaybyspots: Weatherofthedaybyspot[];
  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[];
  
  constructor(private dataService : DataService) { }

  ngOnInit() {
    this.getData_WeatherOfTheDayBySpot_WithDataService();
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
  }

  getData_WeatherOfTheDayBySpot_WithDataService(){
    this.dataService.getData_WeatherOfTheDayBySpot().subscribe((response) =>{
      console.log(response);
      this.weatherofthedaybyspots = response;
    });
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService(){
    this.dataService.getData_ForecastWeatherFor7DaysBySpot().subscribe((response) =>{
      console.log(response);
      this.forecastweatherfor7daysbyspots = response;
    });
  }

}

