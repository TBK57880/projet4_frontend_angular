import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Claim } from 'src/app/entities/claim';
import { User } from 'src/app/entities/user';
import { AdminService } from 'src/app/services/admin.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-admin-users',
  templateUrl: './dashboard-admin-users.component.html',
  styleUrls: ['./dashboard-admin-users.component.css']
})
export class DashboardAdminUsersComponent implements OnInit {

  msg='';

  users: User[] = [];
  claims: Claim[] = [];
  numberOfUsersAccount: number;

  constructor(private userService: UserService, private adminService: AdminService, private router: Router)
  {}
  

  ngOnInit() {
    this.getData_AllUsers_WithAdminService();
  }


  getData_AllUsers_WithAdminService() {
    this.adminService.getAllUsers().subscribe((response: any) => {
      console.log(response);
      this.users = response;
    });
  }


  getData_AllClaims_WithAdminService() {
    this.adminService.getAllClaims().subscribe((response: any) => {
      console.log(response);
      this.users = response;
    });
  }

  getNumberOfUsers_WithAdminService() {
    this.adminService.getNumberOfUsers().subscribe((response: any) => {
      console.log(response);
      this.numberOfUsersAccount = response;
    });
  }


  deleteUser_WithAdminService(id: number) {
    this.adminService.deleteUser(id).subscribe((response: any) => {
      console.log(response);
      // this.numberOfUsersAccount = response;
    });
  }


}
