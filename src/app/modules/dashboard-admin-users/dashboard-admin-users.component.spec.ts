import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminUsersComponent } from './dashboard-admin-users.component';

describe('DashboardAdminUsersComponent', () => {
  let component: DashboardAdminUsersComponent;
  let fixture: ComponentFixture<DashboardAdminUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardAdminUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAdminUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
