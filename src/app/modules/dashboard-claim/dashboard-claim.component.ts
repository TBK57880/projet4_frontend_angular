import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Categoryclaim } from 'src/app/entities/categoryclaim';
import { Claim } from 'src/app/entities/claim';
import { User } from 'src/app/entities/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-claim',
  templateUrl: './dashboard-claim.component.html',
  styleUrls: ['./dashboard-claim.component.css']
})
export class DashboardClaimComponent implements OnInit {

  keys: any[];
  category = Categoryclaim;
  categoryClaimSelected : any;
  claim: Claim;

  
  msg='';

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
  }

  getKeys(obj: any) { return Object.values(obj); }

  addClaim_WithUserService(data: Claim) {
    // console.log(this.address)
    var claim_data = new Claim();
    console.log(data)
    claim_data.title = data.title;
    claim_data.description = data.description;
    claim_data.categoryclaim = data.categoryclaim;

  
    console.log(claim_data);
    this.claim = claim_data;
    this.userService.addClaim(this.userService.currentUser.id_user, claim_data).subscribe((response: any) => {
      console.log(response);
    });
  }

}
