import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardClaimComponent } from './dashboard-claim.component';

describe('DashboardClaimComponent', () => {
  let component: DashboardClaimComponent;
  let fixture: ComponentFixture<DashboardClaimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardClaimComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
