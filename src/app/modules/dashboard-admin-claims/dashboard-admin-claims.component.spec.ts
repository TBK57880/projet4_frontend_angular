import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminClaimsComponent } from './dashboard-admin-claims.component';

describe('DashboardAdminClaimsComponent', () => {
  let component: DashboardAdminClaimsComponent;
  let fixture: ComponentFixture<DashboardAdminClaimsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardAdminClaimsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAdminClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
