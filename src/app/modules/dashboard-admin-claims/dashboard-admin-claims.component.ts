import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Claim } from 'src/app/entities/claim';
import { User } from 'src/app/entities/user';
import { AdminService } from 'src/app/services/admin.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-admin-claims',
  templateUrl: './dashboard-admin-claims.component.html',
  styleUrls: ['./dashboard-admin-claims.component.css']
})
export class DashboardAdminClaimsComponent implements OnInit {
  msg='';

  users: User[] = [];
  claims: Claim[] = [];
  numberOfUsersAccount: number;

  constructor(private userService: UserService, private adminService: AdminService, private router: Router)
  {}
  

  ngOnInit() {
    
    this.getData_AllClaims_WithUserService();
   
  }


  getData_AllClaims_WithUserService() {
    this.adminService.getAllClaims().subscribe((response: any) => {
      console.log(response);
      this.claims = response;
    });
  }

  changeStatusClaimDuringTreatment_WithUserService(id: number, claim: Claim) {
    this.adminService.changeStatusClaimDuringTreatment(id, claim).subscribe((response: any) => {
      console.log(response);
    });
  }

  changeStatusClaimProcessed_WithUserService(id: number, claim: Claim) {
    this.adminService.changeStatusClaimProcessed(id, claim).subscribe((response: any) => {
      console.log(response);
    });
  }

  // getData_AllClaims_WithUserService() {
  //   this.userService.getAllClaims().subscribe((response: any) => {
  //     console.log(response);
  //     this.claims = response;
  //   });
  // }

  // changeStatusClaimDuringTreatment_WithUserService(id: number, claim: Claim) {
  //   this.userService.changeStatusClaimDuringTreatment(id, claim).subscribe((response: any) => {
  //     console.log(response);
  //   });
  // }

  // changeStatusClaimProcessed_WithUserService(id: number, claim: Claim) {
  //   this.userService.changeStatusClaimProcessed(id, claim).subscribe((response: any) => {
  //     console.log(response);
  //   });
  // }


}
