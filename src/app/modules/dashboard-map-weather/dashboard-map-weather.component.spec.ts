import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardMapWeatherComponent } from './dashboard-map-weather.component';

describe('DashboardMapWeatherComponent', () => {
  let component: DashboardMapWeatherComponent;
  let fixture: ComponentFixture<DashboardMapWeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardMapWeatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardMapWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
