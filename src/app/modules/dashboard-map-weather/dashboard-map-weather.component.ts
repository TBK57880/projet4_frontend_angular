import {
  Component,
  OnInit,
  Inject,
  NgZone,
  PLATFORM_ID,
  AfterViewInit,
} from '@angular/core';

import { isPlatformBrowser } from '@angular/common';

// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldHigh from '@amcharts/amcharts4-geodata/worldHigh';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/entities/user';

@Component({
  selector: 'app-dashboard-map-weather',
  templateUrl: './dashboard-map-weather.component.html',
  styleUrls: ['./dashboard-map-weather.component.css'],
})
export class DashboardMapWeatherComponent implements OnInit {

  msg='';
  
  private chart: am4charts.XYChart;

  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[] = [];
  selectOptions: any[] = [];
  selected: number = 0;
  datetime: String;

  table_visibility: any[] = [];

  test1 = true;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private zone: NgZone,
    private dataService: DataService,
    private userService: UserService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
    if(!this.userService.currentUser){
      this.router.navigate(['/registration-login']);
    } else {
      this.router.navigate(['/dashboard/chart-temp-daily'])
    }
    
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService() {
    this.dataService
      .getData_ForecastWeatherFor7DaysBySpot()
      .subscribe((response) => {
        // console.log(response);
        this.forecastweatherfor7daysbyspots = response;

          this.tableVisibilityChanged();
          console.log(this.table_visibility[0]);



        for (
          var i = 0;
          i < this.forecastweatherfor7daysbyspots[0].forecast_weather.length;
          i++
        ) {
          this.selectOptions[i] = {
            name: this.forecastweatherfor7daysbyspots[0].forecast_weather[i]
              .date_time,
            id: i,
          };
          // console.log(this.selectOptions[i].value);
        }
        this.createMaps(this.forecastweatherfor7daysbyspots, this.selected);
        this.datetime =
          this.forecastweatherfor7daysbyspots[0].forecast_weather[
            this.selected
          ].date_time;
      });
  }

  tableVisibilityChanged(){
    if(this.userService.currentUser.favorite0_isVisible == true) {
      this.table_visibility[0] = 'v';
    } else {
      this.table_visibility[0] = 'nv'
    }

    if(this.userService.currentUser.favorite1_isVisible == true) {
      this.table_visibility[1] = 'v';
    } else {
      this.table_visibility[1] = 'nv'
    }

    if(this.userService.currentUser.favorite2_isVisible == true) {
      this.table_visibility[2] = 'v';
    } else {
      this.table_visibility[2] = 'nv'
    }
  }

  changeOption(id: number) {
    // console.log(id);
    // console.log(this.selected);
    this.createMaps(this.forecastweatherfor7daysbyspots, id);
    this.datetime =
      this.forecastweatherfor7daysbyspots[0].forecast_weather[id].date_time;
  }

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  createMaps(
    _forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[],
    id: number
  ) {
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);
      let chart = am4core.create('chartdiv_map', am4maps.MapChart);
      chart.geodata = am4geodata_worldHigh;
      chart.projection = new am4maps.projections.Mercator();

      chart.homeZoomLevel = 6;
      chart.homeGeoPoint = { longitude: 10, latitude: 51 };

      let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
      polygonSeries.exclude = ['AQ'];
      polygonSeries.useGeodata = true;
      polygonSeries.nonScalingStroke = true;
      polygonSeries.strokeOpacity = 0.5;

      let imageSeries = chart.series.push(new am4maps.MapImageSeries());
      let imageTemplate = imageSeries.mapImages.template;
      imageTemplate.propertyFields.longitude = 'longitude';
      imageTemplate.propertyFields.latitude = 'latitude';
      imageTemplate.nonScaling = true;

      let image = imageTemplate.createChild(am4core.Image);
      image.propertyFields.href = 'imageURL';
      image.width = 50;
      image.height = 50;
      image.horizontalCenter = 'middle';
      image.verticalCenter = 'middle';

      let label = imageTemplate.createChild(am4core.Label);
      label.text = '{label}';
      label.horizontalCenter = 'middle';
      label.verticalCenter = 'top';
      label.dy = 20;

      if (this.test1 == true) {
        imageSeries.data = [
          {
            latitude: _forecastweatherfor7daysbyspots[0].lat,
            longitude: _forecastweatherfor7daysbyspots[0].lon,
            imageURL:
              'https://www.amcharts.com/lib/images/weather/animated/rainy-1.svg',
            width: 32,
            height: 32,
            label:
              _forecastweatherfor7daysbyspots[0].spot +
              ' : ' +
              _forecastweatherfor7daysbyspots[0].forecast_weather[id].temp_day +
              '°C \n' +
              _forecastweatherfor7daysbyspots[0].forecast_weather[id]
                .description,
          },
          {
            latitude: _forecastweatherfor7daysbyspots[1].lat,
            longitude: _forecastweatherfor7daysbyspots[1].lon,
            imageURL:
              'https://www.amcharts.com/lib/images/weather/animated/thunder.svg',
            width: 32,
            height: 32,
            label:
              _forecastweatherfor7daysbyspots[1].spot +
              ' : ' +
              _forecastweatherfor7daysbyspots[1].forecast_weather[id].temp_day +
              '°C \n' +
              _forecastweatherfor7daysbyspots[1].forecast_weather[id]
                .description,
          },
          {
            latitude: _forecastweatherfor7daysbyspots[2].lat,
            longitude: _forecastweatherfor7daysbyspots[2].lon,
            imageURL:
              'https://www.amcharts.com/lib/images/weather/animated/cloudy-day-1.svg',
            width: 32,
            height: 32,
            label:
              _forecastweatherfor7daysbyspots[2].spot +
              ' : ' +
              _forecastweatherfor7daysbyspots[2].forecast_weather[id].temp_day +
              '°C \n' +
              _forecastweatherfor7daysbyspots[2].forecast_weather[id]
                .description,
          },
        ];
      }

      chart.svgContainer.resizeSensor.reset();
    });
  }

  ngOnDestroy() {
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
