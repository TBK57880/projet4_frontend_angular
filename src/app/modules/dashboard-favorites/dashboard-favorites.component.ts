import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { Hotels } from 'src/app/entities/hotels';
import { User } from 'src/app/entities/user';
import { Weatherofthedaybyspot } from 'src/app/entities/weatherofthedaybyspot';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-favorites',
  templateUrl: './dashboard-favorites.component.html',
  styleUrls: ['./dashboard-favorites.component.css']
})
export class DashboardFavoritesComponent implements OnInit {

  
  msg='';
  isVisible0 = false;
  isVisible1 = false;
  isVisible2 = false;

  listHotelsByUser: Hotels[] = [];


  // weatherofthedaybyspots: Weatherofthedaybyspot[];
  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[];
  constructor(private dataService : DataService, private userService: UserService, private router: Router){}

  ngOnInit() {
    if(!this.userService.currentUser){
      this.router.navigate(['/registration-login']);
    }
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
    this.isVisible0 = this.userService.currentUser.favorite0_isVisible;
    this.isVisible1 = this.userService.currentUser.favorite1_isVisible;
    this.isVisible2 = this.userService.currentUser.favorite2_isVisible;
    // this.getHotelsByUser_WithUserService();
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService(){
    this.dataService.getData_ForecastWeatherFor7DaysBySpot().subscribe((response) =>{
      // console.log(response);
      this.forecastweatherfor7daysbyspots = response;
    });
  }

  logOut(){
    this.userService.logOut().subscribe(data => {
      console.log("logout");
      this.router.navigate(['/registration-login']);
    },
    error => {
      console.log("exception occured");
      this.msg="Bad move";
    });
  }

  isNotVisibleFavorite0_WithUserService(){
    if(!this.userService.currentUser){
    this.router.navigate(['/registration-login']);
    } else {
      this.userService.isNotVisibleFavorite0(this.userService.currentUser.id_user, this.userService.currentUser).subscribe((response) =>{
        console.log(response);
        // this.userService.currentUser = response;
      });
      this.isVisible0 = false;
    }
}

isNotVisibleFavorite1_WithUserService(){
  if(!this.userService.currentUser){
  this.router.navigate(['/registration-login']);
  } else {
    this.userService.isNotVisibleFavorite1(this.userService.currentUser.id_user, this.userService.currentUser).subscribe((response) =>{
      console.log(response);
    });
    this.isVisible1 = false;
  }
}

isNotVisibleFavorite2_WithUserService(){
if(!this.userService.currentUser){
this.router.navigate(['/registration-login']);
} else {
  this.userService.isNotVisibleFavorite2(this.userService.currentUser.id_user, this.userService.currentUser).subscribe((response) =>{
    console.log(response);
  });
  this.isVisible2 = false;
}
}

// getHotelsByUser_WithUserService() {
//   // console.log(this.userService.currentUser.id_user);
//   // console.log(this.userService.currentUser.phone);
//   this.userService.fetchHotelsUsersByUsername(this.userService.currentUser.userName).subscribe((response: any) => {
//     console.log(response);
//     this.listHotelsByUser = response;
//     console.log(this.listHotelsByUser);
//   });
// }

}
