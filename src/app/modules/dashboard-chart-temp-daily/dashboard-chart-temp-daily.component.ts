import { Component, Inject, NgZone, OnInit, PLATFORM_ID } from '@angular/core';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldHigh from '@amcharts/amcharts4-geodata/worldHigh';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { isPlatformBrowser } from '@angular/common';
import { percent } from '@amcharts/amcharts4/core';
import { User } from 'src/app/entities/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-chart-temp-daily',
  templateUrl: './dashboard-chart-temp-daily.component.html',
  styleUrls: ['./dashboard-chart-temp-daily.component.css']
})
export class DashboardChartTempDailyComponent implements OnInit {

  private chart: am4charts.XYChart;

  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[] = [];
  spot_SelectOptions: any[] = [];
  spot_selected: number = 0;
  msg='';
  

  table_visibility: any[] = [];

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private zone: NgZone,
    private dataService: DataService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
      if(!this.userService.currentUser){
      this.router.navigate(['/registration-login']);
    } else {
      this.router.navigate(['/dashboard/chart-temp-daily'])
    }
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService() {
    this.dataService
      .getData_ForecastWeatherFor7DaysBySpot()
      .subscribe((response) => {
        this.forecastweatherfor7daysbyspots = response;


          // console.log(this.currentUser.favorite0_isVisible);
          // console.log(this.currentUser.favorite1_isVisible);
          // console.log(this.currentUser.favorite2_isVisible);



          this.tableVisibilityChanged();
          // console.log(this.table_visibility[0]);

          for (var i = 0; i < this.forecastweatherfor7daysbyspots.length;i++) {
            if (this.table_visibility[i] == "v" ) {
              this.spot_SelectOptions[i] = {
                name: this.forecastweatherfor7daysbyspots[i].spot,
                id: i,
              };
               console.log(this.spot_SelectOptions[i]);
            } else {
              this.spot_SelectOptions[i] = {
                name: this.msg,
                id: i,
              };
              // console.log(this.spot_SelectOptions[i]);
            }
            
          }
          this.createChart(this.forecastweatherfor7daysbyspots, this.spot_selected);

      });
  }


  changeOptionSpot(_id_spot: number) {
      console.log(this.spot_SelectOptions[_id_spot]);
      if (_id_spot == this.spot_SelectOptions[_id_spot].id && this.spot_SelectOptions[_id_spot].name != this.msg) {
        this.createChart(this.forecastweatherfor7daysbyspots, _id_spot);
      }
  }

  tableVisibilityChanged(){
    if(this.userService.currentUser.favorite0_isVisible == true) {
      this.table_visibility[0] = 'v';
    } else {
      this.table_visibility[0] = 'nv'
    }

    if(this.userService.currentUser.favorite1_isVisible == true) {
      this.table_visibility[1] = 'v';
    } else {
      this.table_visibility[1] = 'nv'
    }

    if(this.userService.currentUser.favorite2_isVisible == true) {
      this.table_visibility[2] = 'v';
    } else {
      this.table_visibility[2] = 'nv'
    }
  }

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  createChart(_forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[], _id_spot: number) {
    this.browserOnly(() => {
  
am4core.useTheme(am4themes_animated);




let chart = am4core.create('chartdiv_chart', am4charts.XYChart)
chart.colors.step = 2;

chart.legend = new am4charts.Legend()
chart.legend.position = 'top'
chart.legend.paddingBottom = 20
chart.legend.labels.template.maxWidth = 95

let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
xAxis.dataFields.category = 'date_time'
xAxis.renderer.cellStartLocation = 0.1
xAxis.renderer.cellEndLocation = 0.9
xAxis.renderer.grid.template.location = 0;

let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
yAxis.min = -10;

var i = 0;

function createSeries(value, name) {
    let series = chart.series.push(new am4charts.ColumnSeries())
    series.dataFields.valueY = value
    series.dataFields.categoryX = 'date_time'
    series.name = name

    series.events.on("hidden", arrangeColumns);
    series.events.on("shown", arrangeColumns);

    let bullet = series.bullets.push(new am4charts.LabelBullet())
    bullet.interactionsEnabled = false
    bullet.dy = 30;
    bullet.label.text = '{valueY}'
    bullet.label.fill = am4core.color('#ffffff')

    return series;
}

_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+1].temp_min;

chart.data = [
    {
        date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i].date_time,
        temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i].temp_min,
        temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i].temp_day,
        temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i].temp_max
    },
    {
      date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+1].date_time,
      temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+1].temp_min,
      temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+1].temp_day,
      temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+1].temp_max
    },
    {
      date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+2].date_time,
      temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+2].temp_min,
      temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+2].temp_day,
      temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+2].temp_max
    },
    {
      date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+3].date_time,
      temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+3].temp_min,
      temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+3].temp_day,
      temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+3].temp_max
    },
    {
      date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+4].date_time,
      temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+4].temp_min,
      temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+4].temp_day,
      temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+4].temp_max
    },
    {
      date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+5].date_time,
      temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+5].temp_min,
      temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+5].temp_day,
      temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+5].temp_max
    },
    {
      date_time: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+6].date_time,
      temp_min: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+6].temp_min,
      temp_day: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+6].temp_day,
      temp_max: _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[i+6].temp_max
    }
]


createSeries('temp_min', 'Minimum Temperature');
createSeries('temp_day', 'Day Temperature');
createSeries('temp_max', 'Maximum Temperature');

function arrangeColumns() {

    let series = chart.series.getIndex(0);

    let w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
    if (series.dataItems.length > 1) {
        let x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
        let x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
        let delta = ((x1 - x0) / chart.series.length) * w;
        if (am4core.isNumber(delta)) {
            let middle = chart.series.length / 2;

            let newIndex = 0;
            chart.series.each(function(series) {
                if (!series.isHidden && !series.isHiding) {
                    series.dummyData = newIndex;
                    newIndex++;
                }
                else {
                    series.dummyData = chart.series.indexOf(series);
                }
            })
            let visibleCount = newIndex;
            let newMiddle = visibleCount / 2;

            chart.series.each(function(series) {
                let trueIndex = chart.series.indexOf(series);
                let newIndex = series.dummyData;

                let dx = (newIndex - trueIndex + middle - newMiddle) * delta

                series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
            })
        }
    }
}
    });
  }

  ngOnDestroy() {
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

}
