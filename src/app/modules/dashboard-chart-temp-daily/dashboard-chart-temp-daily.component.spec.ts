import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardChartTempDailyComponent } from './dashboard-chart-temp-daily.component';

describe('DashboardChartTempDailyComponent', () => {
  let component: DashboardChartTempDailyComponent;
  let fixture: ComponentFixture<DashboardChartTempDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardChartTempDailyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardChartTempDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
