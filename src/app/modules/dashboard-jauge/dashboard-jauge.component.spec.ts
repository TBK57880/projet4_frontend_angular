import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardJaugeComponent } from './dashboard-jauge.component';

describe('DashboardJaugeComponent', () => {
  let component: DashboardJaugeComponent;
  let fixture: ComponentFixture<DashboardJaugeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardJaugeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardJaugeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
