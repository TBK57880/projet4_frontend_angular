import { Component, Inject, NgZone, OnInit, PLATFORM_ID } from '@angular/core';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldHigh from '@amcharts/amcharts4-geodata/worldHigh';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
import { Forecastweatherfor7daysbyspot } from 'src/app/entities/forecastweatherfor7daysbyspot';
import { isPlatformBrowser } from '@angular/common';
import { percent } from '@amcharts/amcharts4/core';
import { User } from 'src/app/entities/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-jauge',
  templateUrl: './dashboard-jauge.component.html',
  styleUrls: ['./dashboard-jauge.component.css'],
})
export class DashboardJaugeComponent implements OnInit {

  
  msg='';
  
  private chart: am4charts.XYChart;

  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[] = [];
  spot_SelectOptions: any[] = [];
  date_SelectOptions: any[] = [];
  date_selected: number = 0;
  spot_selected: number = 0;
  datetime: String;

  test1 = true;
  table_visibility: any[] = [];
  weather_card: any;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private zone: NgZone,
    private dataService: DataService,
    private userService: UserService,
  ) {}

  ngOnInit() {
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
  }

  getData_ForecastWeatherFor7DaysBySpot_WithDataService() {
    this.dataService
      .getData_ForecastWeatherFor7DaysBySpot()
      .subscribe((response) => {
        // console.log(response);
        this.forecastweatherfor7daysbyspots = response;

        // date_SelectOptions
        for (var i = 0; i < this.forecastweatherfor7daysbyspots[0].forecast_weather.length;i++) {
          this.date_SelectOptions[i] = {
            name: this.forecastweatherfor7daysbyspots[0].forecast_weather[i]
              .date_time,
            id: i,
          };
          // console.log(this.date_SelectOptions[i].value);
        }
        this.datetime =
          this.forecastweatherfor7daysbyspots[0].forecast_weather[
            this.date_selected
          ].date_time;

          // spot_SelectOptions

          for (var i = 0; i < this.forecastweatherfor7daysbyspots.length;i++) {
            this.spot_SelectOptions[i] = {
              name: this.forecastweatherfor7daysbyspots[i].spot,
              id: i,
            };
            // console.log(this.spot_SelectOptions[i].value);
          }


          this.weather_card = {
            spot: this.forecastweatherfor7daysbyspots[0].spot,
            date : this.forecastweatherfor7daysbyspots[0].forecast_weather[0].date_time,
            temp : this.forecastweatherfor7daysbyspots[0].forecast_weather[0].temp_day,
            hum : this.forecastweatherfor7daysbyspots[0].forecast_weather[0].humidity,
            wind : this.forecastweatherfor7daysbyspots[0].forecast_weather[0].wind_speed,
            description : this.forecastweatherfor7daysbyspots[0].forecast_weather[0].description,
          }

          // console.log(this.currentUser.favorite0_isVisible);
          // console.log(this.currentUser.favorite1_isVisible);
          // console.log(this.currentUser.favorite2_isVisible);
          this.tableVisibilityChanged();
          console.log(this.table_visibility[0]);
          var percent = this.calculatePercentsJauge(this.forecastweatherfor7daysbyspots, this.date_selected, this.spot_selected);
          this.createJauge(percent);

      });
  }


  tableVisibilityChanged(){
    if(this.userService.currentUser.favorite0_isVisible == true) {
      this.table_visibility[0] = 'v';
    } else {
      this.table_visibility[0] = 'nv'
    }

    if(this.userService.currentUser.favorite1_isVisible == true) {
      this.table_visibility[1] = 'v';
    } else {
      this.table_visibility[1] = 'nv'
    }

    if(this.userService.currentUser.favorite2_isVisible == true) {
      this.table_visibility[2] = 'v';
    } else {
      this.table_visibility[2] = 'nv'
    }
  }

  setDataFromWeatherCard(_forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[], _id_date: number, _id_spot: number){
    this.weather_card = {
      spot: this.forecastweatherfor7daysbyspots[_id_spot].spot,
      date : this.forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].date_time,
      temp : this.forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day,
      hum : this.forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].humidity,
      wind : this.forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].wind_speed,
      description : this.forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description,
    }
  }
  

  calculatePercentsJauge(_forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[], _id_date: number, _id_spot: number) {
    // var table_percents_Tongariro = [];
    var percent = 0;
    // for (let i = 0; i < _forecastweatherfor7daysbyspots[_id_spot].forecast_weather.length; i++) {

      if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day > 5 && _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day <= 10) {
        percent += 5;
      } else if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day > 10 && _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day <= 15) {
        percent += 10;
      } else if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day > 15 && _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day <= 20) {
        percent += 15;
      } else if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day > 20 && _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day <= 25) {
        percent += 20;
      } else if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day > 25 && _forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day <= 30) {
        percent += 25;
      } else if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].temp_day > 30) {
        percent += 30;
      }

       if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].weather_main.includes("Rain")) {
          percent += 10;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("Clouds")) {
          percent += 20;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("Clear")) {
          percent += 30;
       }


        if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("light rain")) {
          percent += 10;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("few clouds")) {
          percent += 15;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("broken clouds")) {
          percent += 20;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("scattered clouds")) {
          percent += 25;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("overcast clouds")) {
          percent += 30;
        } else  if(_forecastweatherfor7daysbyspots[_id_spot].forecast_weather[_id_date].description.includes("clear sky")) {
          percent += 40;
       }
    return percent;
    
  // }

}

  changeOptionDate(_id_date: number) {
    // console.log(_id_date);
    // console.log(this.date_selected);
    // console.log(this.spot_selected);
    var percent = this.calculatePercentsJauge(this.forecastweatherfor7daysbyspots, _id_date, this.spot_selected);
    // console.log(percent);
    this.setDataFromWeatherCard(this.forecastweatherfor7daysbyspots, _id_date, this.spot_selected);
    this.createJauge(percent);
    this.datetime =
    this.forecastweatherfor7daysbyspots[0].forecast_weather[_id_date].date_time;
  }

  changeOptionSpot(_id_spot: number) {
    // console.log(_id_spot);
    // console.log(this.spot_selected);
    // console.log(this.date_selected);
    var percent = this.calculatePercentsJauge(this.forecastweatherfor7daysbyspots, this.date_selected, _id_spot);
    // console.log(percent);
    this.setDataFromWeatherCard(this.forecastweatherfor7daysbyspots, this.date_selected, _id_spot);
    this.createJauge(percent);
  }

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  createJauge(_percent: number) {
    this.browserOnly(() => {
      /* Chart code */
      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      // create chart
      let chart = am4core.create('chartdiv_jauge', am4charts.GaugeChart);
      chart.innerRadius = am4core.percent(82);

      /**
       * Normal axis
       */

      let axis = chart.xAxes.push(
        new am4charts.ValueAxis<am4charts.AxisRendererCircular>()
      );
      axis.min = 0;
      axis.max = 100;
      axis.strictMinMax = true;
      axis.renderer.radius = am4core.percent(80);
      axis.renderer.inside = true;
      axis.renderer.line.strokeOpacity = 1;
      axis.renderer.ticks.template.disabled = false;
      axis.renderer.ticks.template.strokeOpacity = 1;
      axis.renderer.ticks.template.length = 10;
      axis.renderer.grid.template.disabled = true;
      axis.renderer.labels.template.radius = 40;
      axis.renderer.labels.template.adapter.add('text', function (text) {
        return text + '%';
      });

      /**
       * Axis for ranges
       */

      let colorSet = new am4core.ColorSet();

      let axis2 = chart.xAxes.push(
        new am4charts.ValueAxis<am4charts.AxisRendererCircular>()
      );
      axis2.min = 0;
      axis2.max = 100;
      axis2.strictMinMax = true;
      axis2.renderer.labels.template.disabled = true;
      axis2.renderer.ticks.template.disabled = true;
      axis2.renderer.grid.template.disabled = true;

      let range0 = axis2.axisRanges.create();
      range0.value = 0;
      range0.endValue = 50;
      range0.axisFill.fillOpacity = 1;
      range0.axisFill.fill = colorSet.getIndex(0);

      let range1 = axis2.axisRanges.create();
      range1.value = 50;
      range1.endValue = 100;
      range1.axisFill.fillOpacity = 1;
      range1.axisFill.fill = colorSet.getIndex(2);

      /**
       * Label
       */

      let label = chart.radarContainer.createChild(am4core.Label);
      label.isMeasured = false;
      label.fontSize = 45;
      label.x = am4core.percent(50);
      label.y = am4core.percent(100);
      label.horizontalCenter = 'middle';
      label.verticalCenter = 'bottom';
      label.text = '50%';

      /**
       * Hand
       */

      let hand = chart.hands.push(new am4charts.ClockHand());
      hand.axis = axis2;
      hand.innerRadius = am4core.percent(20);
      hand.startWidth = 10;
      hand.pin.disabled = true;
      hand.value = 50;

      hand.events.on('propertychanged', function (ev) {
        range0.endValue = ev.target.value;
        range1.value = ev.target.value;
        label.text = axis2.positionToValue(hand.currentPosition).toFixed(1);
        axis2.invalidate();
      });

      let value = _percent;
      let animation = new am4core.Animation(
        hand,
        {
          property: 'value',
          to: value,
        },
        5000,
        am4core.ease.cubicOut
        ).start();



      // setInterval(function () {
      //   let value = Math.round(Math.random() * 100);
      //   let animation = new am4core.Animation(
      //     hand,
      //     {
      //       property: 'value',
      //       to: value,
      //     },
      //     1000,
      //     am4core.ease.cubicOut
      //   ).start();
      // }, 2000);
    });
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
