import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/entities/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard-settings',
  templateUrl: './dashboard-settings.component.html',
  styleUrls: ['./dashboard-settings.component.css']
})
export class DashboardSettingsComponent implements OnInit {

  user: User;

  msg='';

  constructor(public userService: UserService, private router: Router) {
    
  }

  ngOnInit() {
    
  }

  logOut(){
    this.userService.logOut().subscribe(data => {
      console.log("logout");
      this.router.navigate(['/registration-login']);
    },
    error => {
      console.log("exception occured");
      this.msg="Bad move";
    });
  }

  updateUser_WithUserService() {
    this.userService.updateUser(this.userService.currentUser.id_user, this.userService.currentUser).subscribe((response: any) => {
      console.log(response);
    });
  }

  updatePassword_WithUserService(user: User) {
    console.log(user.password);
    this.userService.currentUser.password = user.password;
    this.userService.updatePasswordUser(this.userService.currentUser.id_user, user).subscribe((response: any) => {
      console.log(response);
    });
  }


}
