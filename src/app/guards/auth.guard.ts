import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from '../services/user.service';
import {User} from '../entities/user';

@Injectable({
    providedIn: 'root'
  })
  export class AuthGuard implements CanActivate {
  
    constructor(private router: Router, private userService: UserService) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if(this.userService.currentUser) {
        if(route.data.roles && route.data.roles.indexOf(this.userService.currentUser.role) === -1){
          this.router.navigate(['/401']);
          return false;
        }
        return true;
      }
      this.router.navigate(['/registration-login']);
      return false;
    }
  }