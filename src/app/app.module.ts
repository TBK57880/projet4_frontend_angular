import { DefaultModule } from './layouts/default/default.module';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
// import { SwiperModule } from 'swiper/angular';
// import { SwiperModule } from 'ngx-swiper-wrapper';
// import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
// import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationLoginComponent } from './registration-login/registration-login.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UnathorizedComponent } from './unathorized/unathorized/unathorized.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdminService } from './services/admin.service';
import { UserService } from './services/user.service';
import { AuthGuard } from './guards/auth.guard';
import { SliderSpotsComponent } from './slider-spots/slider-spots.component';
import { IsAdminGuard } from './guards/is-admin.guard';




@NgModule({
  declarations: [
    AppComponent,
    RegistrationLoginComponent,
    HomeComponent,
    UnathorizedComponent,
    NotFoundComponent,
    SliderSpotsComponent,
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DefaultModule,
    // SwiperModule
  ],
  providers: [AdminService, UserService, AuthGuard, IsAdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
