import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderSpotsComponent } from './slider-spots.component';

describe('SliderSpotsComponent', () => {
  let component: SliderSpotsComponent;
  let fixture: ComponentFixture<SliderSpotsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SliderSpotsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderSpotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
