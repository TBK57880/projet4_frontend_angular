import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import gsap from 'gsap';

import { Forecastweatherfor7daysbyspot } from '../entities/forecastweatherfor7daysbyspot';
import { User } from '../entities/user';
import { DataService } from '../services/data.service';
import { SliderService } from '../services/slider.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-slider-spots',
  templateUrl: './slider-spots.component.html',
  styleUrls: ['./slider-spots.component.css']
})
export class SliderSpotsComponent implements OnInit, AfterViewInit{

  currentSpot: "Dubrovnik" | "Matmata" | "Tongariro" = "Dubrovnik";
  showPopUp = false;
  showMenu = false;
  forecastweatherfor7daysbyspots: Forecastweatherfor7daysbyspot[];
  currentUser: User;
  msg='';
  iframe_src1: string | SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[0]);
  iframe_src2: string | SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[1]);



  constructor(private router: Router, private activatedRoute: ActivatedRoute, 
    private dataService : DataService, private userService: UserService, private sliderService: SliderService, private sanitizer: DomSanitizer) { 
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
  ngAfterViewInit(): void {
    this.scrollAnimation();
  }

  ngOnInit() {
    this.getData_ForecastWeatherFor7DaysBySpot_WithDataService();
    //   if(!this.currentUser){
    //   this.router.navigate(['/registration-login']);
    // }
  }


  getData_ForecastWeatherFor7DaysBySpot_WithDataService(){
    this.dataService.getData_ForecastWeatherFor7DaysBySpot().subscribe((response) =>{
      console.log(response);
      this.forecastweatherfor7daysbyspots = response;
    });
  }

  isVisibleFavorite0_WithUserService(){
      if(!this.currentUser){
      this.router.navigate(['/registration-login']);
      } else {
        this.userService.isVisibleFavorite0(this.currentUser.id_user, this.currentUser).subscribe((response) =>{
          console.log(response);
        });
      }
  }

  isVisibleFavorite1_WithUserService(){
    if(!this.currentUser){
    this.router.navigate(['/registration-login']);
    } else {
      this.userService.isVisibleFavorite1(this.currentUser.id_user, this.currentUser).subscribe((response) =>{
        console.log(response);
      });
    }
}

isVisibleFavorite2_WithUserService(){
  if(!this.currentUser){
  this.router.navigate(['/registration-login']);
  } else {
    this.userService.isVisibleFavorite2(this.currentUser.id_user, this.currentUser).subscribe((response) =>{
      console.log(response);
    });
  }
}

  onToggleClick(){
    this.showMenu = !this.showMenu;
  }

  onTogglePopUp(){
    this.showPopUp = !this.showPopUp;
  }

  onClosePopUp(){
    this.showPopUp = false;
  }

  scrollAnimation(){
  gsap.from('.islands__subtitle', {opacity: 0, duration: .2, delay: .2, y: -20});
  gsap.from('.islands__title', {opacity: 0, duration: .3, delay: .3, y: -20});
  gsap.from('.islands__description', {opacity: 0, duration: .4, delay: .4, y: -20});
  gsap.from('.islands__button', {opacity: 0, duration: .5, delay: .5, y: -20});
  gsap.from('.islands__video-content', {opacity: 0, duration: .6, delay: .6, y: -20});
this.onClosePopUp();
  }

  onDubrovnikClick(){
    this.currentSpot = "Dubrovnik";
    this.scrollAnimation();
    this.iframe_src1 = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[0]);
    this.iframe_src2 = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[1]);
  }

  onMatmataClick(){
    this.currentSpot = "Matmata";
    this.scrollAnimation();
    this.iframe_src1 = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[2]);
    this.iframe_src2 = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[3]);
  }

  onTongariroClick(){
    this.currentSpot = "Tongariro";
    this.scrollAnimation();
    this.iframe_src1 = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[4]);
    this.iframe_src2 = this.sanitizer.bypassSecurityTrustResourceUrl(this.sliderService.linkYoutube[5]);
  }
}
