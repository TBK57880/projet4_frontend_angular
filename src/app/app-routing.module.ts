import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { RegistrationLoginComponent } from './registration-login/registration-login.component';
import { HomeComponent } from './home/home.component';
import { DefaultComponent } from './layouts/default/default.component';
import { PostsComponent } from './modules/posts/posts.component';
import { DashboardFavoritesComponent } from './modules/dashboard-favorites/dashboard-favorites.component';
import {AuthGuard} from './guards/auth.guard';
import {Role} from './entities/role';
import { UnathorizedComponent } from './unathorized/unathorized/unathorized.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DashboardSettingsComponent } from './modules/dashboard-settings/dashboard-settings.component';
import { SliderSpotsComponent } from './slider-spots/slider-spots.component';
import { DashboardHotelsComponent } from './modules/dashboard-hotels/dashboard-hotels.component';
import { DashboardMapWeatherComponent } from './modules/dashboard-map-weather/dashboard-map-weather.component';
import { DashboardMapPlaneComponent } from './modules/dashboard-map-plane/dashboard-map-plane.component';
import { DashboardJaugeComponent } from './modules/dashboard-jauge/dashboard-jauge.component';
import { DashboardChartTempDailyComponent } from './modules/dashboard-chart-temp-daily/dashboard-chart-temp-daily.component';
import { DashboardChartCupHumidityComponent } from './modules/dashboard-chart-cup-humidity/dashboard-chart-cup-humidity.component';
import { DashboardChartTopSpotComponent } from './modules/dashboard-chart-top-spot/dashboard-chart-top-spot.component';
import { DashboardClaimComponent } from './modules/dashboard-claim/dashboard-claim.component';
import { DashboardAdminUsersComponent } from './modules/dashboard-admin-users/dashboard-admin-users.component';
import { DashboardAdminClaimsComponent } from './modules/dashboard-admin-claims/dashboard-admin-claims.component';
import { IsAdminGuard } from './guards/is-admin.guard';

const routes: Routes = [
  {path:'', redirectTo: 'slider-spots', pathMatch:'full'},
  {path:'home', component: HomeComponent},
  {path:'slider-spots', component: SliderSpotsComponent},
  {path:'registration-login', component: RegistrationLoginComponent},
  {path: 'dashboard',
  component: DefaultComponent,
  children: [{
    path: 'posts',
    component: PostsComponent
  },{
    path: 'settings',
    component: DashboardSettingsComponent
  },{
    path: 'claims',
    component: DashboardClaimComponent
  },{
    path: 'map-weather',
    component: DashboardMapWeatherComponent,
  },{
    path: 'map-plane',
    component: DashboardMapPlaneComponent,
  },{
    path: 'favorites',
    component: DashboardFavoritesComponent,
  },{
    path: 'hotels',
    component: DashboardHotelsComponent,
  },{
    path: 'admin-users',
    canActivate: [IsAdminGuard],
    component: DashboardAdminUsersComponent,
  },{
    path: 'admin-claims',
    canActivate: [IsAdminGuard],
    component: DashboardAdminClaimsComponent,
  },{
    path: 'jauge',
    component: DashboardJaugeComponent,
  },{
    path: 'chart-temp-daily',
    component: DashboardChartTempDailyComponent,
  },{
    path: 'chart-cup-humidity',
    component: DashboardChartCupHumidityComponent,
  },{
    path: 'chart-top-spot',
    component: DashboardChartTopSpotComponent,
  }]},
  {path:'404', component: NotFoundComponent},
  {path:'401', component: UnathorizedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true},)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private router: Router) {
    this.router.errorHandler = (error: any) => {
      console.error(error);
      this.router.navigate(['/404']);
    }
  }
 }
