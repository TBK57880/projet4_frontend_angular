import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  video_time_text1: boolean = false;
  video_time_text2: boolean = false;
  video_time_text3: boolean = false;
  video_time_text4: boolean = false;
  video_time_text5: boolean = false;
  video_time_text6: boolean = false;
  video_time_text7: boolean = false;



constructor(private dataService: DataService) {}

  ngOnInit() {
  
    var video = document.getElementById('homeVideo') as HTMLMediaElement;
    var btn_pause = document.getElementById('BtnPausePlay') as HTMLInputElement;
    var btn_vol = document.getElementById('BtnVolume') as HTMLInputElement;
    var icon_pause = document.getElementById('IconPause') as HTMLElement;
    console.log(icon_pause);
    var icon_vol = document.getElementById('IconVolume') as HTMLElement;
    
    btn_pause.addEventListener("click", () => {
      videoPauseOrPLay();
    });

    btn_vol.addEventListener("click", () => {
      videoVolumeChanged();
    });

  
    function videoPauseOrPLay() {
      if (video.paused) {
        video.play();
        btn_pause.innerHTML = 'Pause';
        icon_pause.className = 'bx bx-pause btn-icon'
      } else {
        video.pause();
        btn_pause.innerHTML = 'Play';
        icon_pause.className = 'bx bx-play btn-icon'
      }
      setVideoCurrentTime(video);
    }

    function videoVolumeChanged() {
      if (video.muted == false) {
        video.muted = true;
        btn_vol.innerHTML = 'Muted';
        icon_vol.setAttribute('class', 'bx bx-pause btn-icon');
      } else if (video.muted == true) {
        video.muted = false;
        btn_vol.innerHTML = 'Full';
        icon_vol.setAttribute('class', 'bx bx-volume-full btn-icon');
        console.log(icon_vol.className);
      }
    }

    function setVideoCurrentTime(video: HTMLMediaElement) {
      console.log(video.currentTime);
    }
    

    video.addEventListener("timeupdate", () => {
      if (video.currentTime >= 0.000 && video.currentTime <= 3.000){
        this.video_time_text1=true;
        this.video_time_text2=false;
        this.video_time_text3=false;
        this.video_time_text4=false;
        this.video_time_text5=false;
        this.video_time_text6=false;
        this.video_time_text7=false;

      }
      if (video.currentTime >= 3.000 && video.currentTime <= 5.000){
        this.video_time_text1=false;
        this.video_time_text2=true;
        this.video_time_text3=false;
        this.video_time_text4=false;
        this.video_time_text5=false;
        this.video_time_text6=false;
        this.video_time_text7=false;
      }
      if (video.currentTime >= 5.000 && video.currentTime <= 7.000){
        this.video_time_text1=false;
        this.video_time_text2=false;
        this.video_time_text3=true;
        this.video_time_text4=false;
        this.video_time_text5=false;
        this.video_time_text6=false;
        this.video_time_text7=false;
      }
      if (video.currentTime >= 7.000 && video.currentTime <= 9.000){
        this.video_time_text1=false;
        this.video_time_text2=false;
        this.video_time_text3=false;
        this.video_time_text4=true;
        this.video_time_text5=false;
        this.video_time_text6=false;
        this.video_time_text7=false;
      }
      if (video.currentTime >= 9.000 && video.currentTime <= 11.000){
        this.video_time_text1=false;
        this.video_time_text2=false;
        this.video_time_text3=false;
        this.video_time_text4=false;
        this.video_time_text5=true;
        this.video_time_text6=false;
        this.video_time_text7=false;
      }
      if (video.currentTime >= 11.000 && video.currentTime <= 13.000){
        this.video_time_text1=false;
        this.video_time_text2=false;
        this.video_time_text3=false;
        this.video_time_text4=false;
        this.video_time_text5=false;
        this.video_time_text6=true;
        this.video_time_text7=false;
      }
      if (video.currentTime >= 13.000){
        this.video_time_text1=false;
        this.video_time_text2=false;
        this.video_time_text3=false;
        this.video_time_text4=false;
        this.video_time_text5=false;
        this.video_time_text6=false;
        this.video_time_text7=true;
      }
    })
  }
}
