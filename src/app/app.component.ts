import { AfterViewInit, Component, OnInit } from '@angular/core';
import {slider} from '../assets/js/slider';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FrontEndApp';

  constructor(private dataService : DataService){}
 

  ngOnInit(){
    this.verifyConnectionAPI();
  }
  
  verifyConnectionAPI(){
    this.dataService.verifyConnection().subscribe((response) =>{
      console.log('Response from API is, ' + response)
    }, (error) => {
      console.log('Error is: ' + error);
    })
  }

}

