window.onload=function(){
  /*==================== SHOW MENU ====================*/
const showMenu = (toggleId, navId) =>{
  const toggle = document.getElementById(toggleId),
  nav = document.getElementById(navId)

  // Validate that variables exist
  if(toggle && nav){
      toggle.addEventListener('click', () =>{
          // We add the show-menu class to the div tag with the nav__menu class
          nav.classList.toggle('show-menu')
      })
  }
}

showMenu('nav-toggle','nav-menu')

/*==================== SWIPER JS ====================*/
let galleryThumbs = new Swiper('.gallery-thumbs', {
  spaceBetween: 0,
  slidesPerView: 0,
})

let galleryTop = new Swiper('.gallery-top', {
  effect: 'fade',
  loop: true,

  thumbs: {
    swiper: galleryThumbs
  }
})


/*==================== POPUP ====================*/
const btnOpenVideo = document.querySelectorAll('.islands__video-content');
const islandsPopup = document.getElementById('popup');

function poPup(){
  islandsPopup.classList.add('show-popup')
}
btnOpenVideo.forEach(b => b.addEventListener('click', poPup))

const btnCloseVideo = document.getElementById('popup-close')

btnCloseVideo.addEventListener('click', ()=> {
  islandsPopup.classList.remove('show-popup')
})

/*==================== GSAP ANIMATION ====================*/
const controlImg = document.querySelectorAll('.controls__img')

function scrollAnimation(){
  gsap.from('.islands__subtitle', {opacity: 0, duration: .2, delay: .2, y: -20})
  gsap.from('.islands__title', {opacity: 0, duration: .3, delay: .3, y: -20})
  gsap.from('.islands__description', {opacity: 0, duration: .4, delay: .4, y: -20})
  gsap.from('.islands__button', {opacity: 0, duration: .5, delay: .5, y: -20})
  gsap.from('.islands__video-content', {opacity: 0, duration: .6, delay: .6, y: -20})

  islandsPopup.classList.remove('show-popup')
}

controlImg.forEach(c => c.addEventListener('click', scrollAnimation));

var linkYoutube = new Array();
linkYoutube[0]= "https://www.youtube.com/embed/4tPG4z2HeXg";
linkYoutube[1]= "https://www.youtube.com/embed/rUldAQ2Haqo";

linkYoutube[2]= "https://www.youtube.com/embed/0NwFVXfSCCw";
linkYoutube[3]= "https://www.youtube.com/embed/BsxcNhg4D3Y";

linkYoutube[4]= "https://www.youtube.com/embed/t3sn-EQFVWk";
linkYoutube[5]= "https://www.youtube.com/embed/wy7feYsWWP4";


const imgDubrovnik = document.getElementById('img-control').getElementsByTagName('img')[0];
console.log(imgDubrovnik);
const imgMatmata = document.getElementById('img-control').getElementsByTagName('img')[1];
console.log(imgMatmata);
const imgTongariro = document.getElementById('img-control').getElementsByTagName('img')[2];
console.log(imgTongariro);

const iframe = document.getElementsByTagName('iframe');
console.log(iframe[0]);
console.log(iframe[1]);

imgDubrovnik.addEventListener('click', ()=>{
  var src_1 = linkYoutube[0];
  var src_2 = linkYoutube[1];
  iframe[0].setAttribute("src", src_1);
  iframe[1].setAttribute("src", src_2);
  return;
});

imgMatmata.addEventListener('click', ()=>{
  var src_1 = linkYoutube[2];
  var src_2 = linkYoutube[3];
  iframe[0].setAttribute("src", src_1);
  iframe[1].setAttribute("src", src_2);
  return;
});

imgTongariro.addEventListener('click', ()=>{
  var src_1 = linkYoutube[4];
  var src_2 = linkYoutube[5];
  iframe[0].setAttribute("src", src_1);
  iframe[1].setAttribute("src", src_2);
  return;
});
  
}


